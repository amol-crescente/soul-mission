jQuery(document).ready(function($) { 

	jQuery('#birth_date').datepicker({
        showButtonPanel: true,
        changeMonth: true, 
    	changeYear: true,
        dateFormat: 'mm/dd/yy',
        yearRange: "-100:+0"
        //minDate: 0
    });
});