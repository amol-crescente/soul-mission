jQuery(document).ready(function($) {
  /*===================================
    zone_name dropdown chage
  ===================================*/
  $(document).on("change","#zone_name",function(e) {

      e.preventDefault();
      
      var base_url = $('#base_url').val();
      var zone_name = $('#zone_name').val();
      // console.log('base_url  =>>', base_url);
      // console.log('zone_name  =>>', zone_name);
      window.location.href = base_url+'?zone='+zone_name;      
  });
  
  
});