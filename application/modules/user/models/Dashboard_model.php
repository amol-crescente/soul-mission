<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
* 
*/
class Dashboard_model extends CI_Model
{
	public function save_user($data){
		$user = array(
		 	'full_name'         =>$data['full_name'],
            'birth_date'    	=>date('Y-m-d',$data['birth_date']),
            'birth_time'        =>$data['birth_time'],
            'birth_city'       	=>$data['birth_city'],
            'email'       		=>$data['email']

        );

        if($this->db->insert('users',$user))
        {
            $config             = $this->config->item('ci_astro');
        	$user_id = $this->db->insert_id();
            if(!empty($user_id)){
                /*$user_data = get_object('users','*','user_id',$user_id,1);
                
                $subject   = 'User Data';
                $maildata  = array('full_name'=>!empty($user_data->full_name)?$user_data->full_name:'','sitename'=>$config['site_title'], 'email'=>$user_data->email,'birth_date'=>$user_data->birth_date,'birth_time'=>!empty($user_data->birth_time)?$user_data->birth_time:'','birth_city'=>!empty($user_data->birth_city)?$user_data->birth_city:'');
                $message  = $this->load->view('emails/admin_email.php',$maildata,TRUE);
                $sendmail = $this->astroportal->send_mail('swapnali.crescente1@gmail.com', $subject, $message, '','');*/
            }
            return $user_id;
        }
        else
        {
            return false;
        }

	}
	
	public function get_astro_data($user_id){
		
       	$this->db->select('* FROM `astro_data` WHERE (SELECT birth_date FROM users WHERE user_id="'.$user_id.'") BETWEEN from_date AND to_date');
		$query = $this->db->get();
        //echo $this->db->last_query();die();
		$result = $query->result();
        
		return $result;
	}
}