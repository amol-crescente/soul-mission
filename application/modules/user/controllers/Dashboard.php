<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Dashboard extends CI_Controller {

	function __construct() {
        parent::__construct();
        $this->load->model('dashboard_model');
    }

    public function index()
    {      
        $data['config']         = $this->config->item('ci_astro');
        $data['extrajs']        = array();
        $data['extracss']       = array();
        $data['main_content']   = 'dashboard/index';
        $data['pageclass']      = 'dashboard primary-menu';
        $data['nosidebar']      = false;
        $data['page_title']     = 'Dashboard';
        $data['msg']            = '';
       
        $data['base_url']     = base_url('user/dashboard');

        $this->load->view('layouts/default', $data);
       
    }

    public function  save_user(){
        $postdata = $this->input->post(); 
        $result = $this->dashboard_model->save_user($postdata);
        if(!empty($result))
        {
            //$this->session->set_flashdata('save_user', array('class'=>'success','msg'=>'User is added successfully'));
            redirect('user/dashboard/get_astro_data/'.$result,'refresh');
        
        }else{
             
            $this->session->set_flashdata('save_user', array('class'=>'danger','msg'=>'Error in adding the user'));
                redirect('user/dashboard','refresh');
        }
    }

    public function get_astro_data(){
        if($this->uri->segment(4)>0){
            $data['config']         = $this->config->item('ci_astro');
            $data['extrajs']        = array();
            $data['extracss']       = array();
            $data['user_id']        = $this->uri->segment(4);
            $data['get_name']       = get_object('users','full_name','user_id',$data['user_id']);
            $data['get_astro_data'] = $this->dashboard_model->get_astro_data($data['user_id']);
            $data['main_content']   = 'dashboard/astro_data';
            $data['pageclass']      = 'dashboard primary-menu';
            $data['nosidebar']      = false;
            $data['page_title']     = 'Dashboard';
            $data['msg']            = '';
            $data['base_url']     = base_url('user/dashboard');

            $this->load->view('layouts/default', $data);    
        }else{
             redirect('user/dashboard','refresh');
        }
        
    }

}
