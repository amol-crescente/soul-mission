<!-- Page Wrapper -->
<div id="wrapper">
	<?php $this->load->view('includes/header');?>
	<?php //$this->load->view('includes/aside-left');?>

<!-- Content Wrapper -->
    <div id="content-wrapper" class="d-flex flex-column">
      <!-- Main Content -->
      <div id="content">
		<?php //$this->load->view('includes/topnav');?>


		<?php $this->load->view('includes/aside-right');?>

		<?php $this->load->view('includes/footer');?>

	</div>
	<!-- End Content Wrapper -->
	</div>
	<!-- End Main Content -->

</div>
<!-- End Page Wrapper -->