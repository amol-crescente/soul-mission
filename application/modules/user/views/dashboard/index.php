<!-- Content Row -->
<div class="row">
  <div class="col-sm-6 ">
  	<?php
  	echo !empty($this->session->flashdata('save_user')) ? set_flashmessage($this->session->flashdata('save_user')) : '';
  	$attributes = array('class' =>'form-group', 'id' => 'add_user','method'=>'post','enctype'=>'multipart/form-data','name'=>'add_user','autocomplete'=>'off');
	    	echo form_open('user/dashboard/save_user', $attributes);
  	?>
	  	<div class="form-content">
	  		<div class="form-group">
				<label for="full_name">Full Name</label> 
				<input type="text" name="full_name" id="full_name">
			</div>
			<div class="form-group">
				<label for="birth_date">Date of Birth</label> 
				<span style="color:red">*</span>
				<input type="text" name="birth_date" id="birth_date" required="true">
			</div>
			<div class="form-group">
				<label for="birth_time">Time of Birth</label> 
				
				<input type="text" name="birth_time" id="birth_time">
			</div>
			<div class="form-group">
				<label for="birth_city">City of Birth</label> 
				
				<input type="text" name="birth_city" id="birth_city">
			</div>
			<div class="form-group">
				<label for="email">Email</label> 
				<span style="color:red">*</span>
				<input type="email" name="email" id="email" required="true">
			</div>
			<div class="form-group">
				<input type="submit" name="save_user" class="btn btn-primary" id="save_user" value="Add User">
			</div>
	  	</div>
  	</form>
  </div>
</div>
        