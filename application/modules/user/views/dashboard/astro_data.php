<div class="view-container">
	<div class="row">
		<div class="panel panel-default">
			<div class="form-heading">
    			<h3 class="no-margin text-center">
    				Soul Mission Data
    			</h3>
    		</div>
    		<div class="form-content open-view text-center">
    			<h3>Dear <?php echo $get_name ?>,</h3>
    			<h3>Your soul mission is in...</h3>
    		</div>
    		<div class="text-center">
	    		<?php 
	    		foreach ($get_astro_data as $value) {
	    		?>
	    			<h3><?php echo $value->sign; ?></h3>	
	    			<h3><?php echo $value->title; ?></h3>	
	    			<h3><?php echo $value->body; ?></h3>	
	    		<?php
	    		}
	    		?>
    		</div>
		</div>
	</div>
</div>