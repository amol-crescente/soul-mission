<?php

defined('BASEPATH') OR exit('No direct script access allowed');

/*

| -------------------------------------------------------------------------

| CI Bootstrap 3 Configuration

| -------------------------------------------------------------------------

| This file lets you define default values to be passed into views 

| when calling MY_Controller's render() function. 

| 

| See example and detailed explanation from:

| 	/application/config/ci_animal_example.php

*/

$config['ci_astro'] = array(

	// Site name

	'site_name' => 'Astro',

	'site_title' => 'Astro Data',

	// Default page title prefix

	'page_title_prefix' => '',

	// Default page title

	'page_title' => '',

	// Default meta data

	'meta_data'	=> array(

		'author'		=> '',

		'description'	=> '',

		'keywords'		=> ''

	),

	// Default scripts to embed at page head or end

	'scripts' => array(

		'head'	=> array(

		),

		'foot'	=> array(

			'assets/js/jquery.min.js',
			'assets/js/bootstrap.min.js',
			'assets/js/plugins/datepicker/jquery-ui.js',
			'assets/js/custom-ajax.js',
			'assets/js/custom.js'
			
		),

	),

	// Default stylesheets to embed at page head

	'stylesheets' => array(

		'screen' => array(
			'assets/css/bootstrap.min.css',
			'assets/js/plugins/datepicker/jquery-ui.css',
            'assets/css/style.css'
		)

	),

	// Default CSS class for <body> tag

	'body_class' => ' skin-black ',

	

	// Multilingual settings

	'languages' => array(

		'default'		=> 'en',

		'autoload'		=> array('general'),

		'available'		=> array(

			'en' => array(

				'label'	=> 'English',

				'value'	=> 'english'

			)

		)

	),

	// Google Analytics User ID

	'ga_id' => '',

	// Menu items

	'adminmenu' => array(

		'dashboard' => array(

			'name'		=> 'ta_menu_dashboard',

			'url'		=> 'admin/dashboard',

            'icon'		=> 'fa-tachometer-alt'

		),

	),	

	// Login page

	'login_url' => 'auth',

	// Restricted pages

	'page_auth' => array(

	),

	// Email config

	'email' => array(
		'from_main_email'   => 'kheratkar.nikita@crescenteservices.com',
		'from_name'			=> 'Soul Mission',
		'subject_prefix'	=> 'Mail From Soul Mission',
		
		// Mailgun HTTP API
		'mailgun_api'		=> array(
			'domain'			=> '',
			'private_api_key'	=> ''
		),
	),

    
    // Debug tools

	'debug' => array(

		'view_data'	=> FALSE,

		'profiler'	=> FALSE

	)

);

/*

| -------------------------------------------------------------------------

| Override values from /application/config/config.php

| -------------------------------------------------------------------------

*/

$config['sess_cookie_name'] = 'ci_session_frontend';