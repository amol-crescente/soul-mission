<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Getlist_lib {
    private $CI;
    public function __construct($params = array())
    {
        $this->CI =& get_instance();
        $this->CI->load->database(); 
    }
    public function getAutoid($table){
        $sql = "SELECT `AUTO_INCREMENT` as id FROM  INFORMATION_SCHEMA.TABLES WHERE TABLE_SCHEMA = '".$this->CI->db->database."' AND TABLE_NAME = '".$table."'";
        $results = $this->CI->db->query($sql);
        $setdata = $results->row();
        $autoid = $setdata->id;
        return $autoid;       
    }
    public function getListings($listarray){
        $account_id = $this->CI->session->userdata('account_id');
        if(in_array('kiosklist',$listarray))
        {
        $sql     = "SELECT acc_kiosk_id,account_id,identifier FROM ".TB_ACCOUNT_KIOSKS." WHERE account_id=$account_id AND status='active' ORDER BY identifier ASC";
        $results = $this->CI->db->query($sql);
        $listings['kiosklist'] = $results->result_array();
        }
        //subscription list
        if(in_array('subscriptionlist',$listarray))
        {
        $sql     = "SELECT subscription_id,subscription_name FROM ".TB_SUBSCRIPTIONS." ";
        $results = $this->CI->db->query($sql);
        $listings['subscriptionlist'] = $results->result_array();
        }
        //params list
        if(in_array('paramslist',$listarray))
        {
        $sql     = "SELECT `parameter_id`,`key`,`value` FROM ".TB_PARAMETERS." WHERE account_id=$account_id ORDER BY parameter_id ASC";
        $results = $this->CI->db->query($sql);
        $listings['paramslist'] = $results->result_array();
        }
        return $listings;       
    }

    public function getGlobalListings($listarray){
        $account_id = $this->CI->session->userdata('account_id');
        //params list
        if(in_array('paramslist',$listarray))
        {
        $sql     = "SELECT `parameter_id`,`key`,`value` FROM ".TB_PARAMETERS." WHERE account_id IS NULL ORDER BY parameter_id ASC";
        // echo $sql; echo "\n - ";
        $results = $this->CI->db->query($sql);
       
        $globallistings['paramslist'] = $results->result_array();
        // print_r($listings); die;
        }
        return $globallistings;       
    }
}