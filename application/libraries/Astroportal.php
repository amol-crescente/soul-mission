<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/**
 * Format class
 * Help convert between various formats such as XML, JSON, CSV, etc.
 *
 * @author    Phil Sturgeon, Chris Kacerguis, @softwarespot
 * @license   http://www.dbad-license.org/
 */
class Astroportal {
    private $CI;
    public function __construct($params = array())
    {
        $this->CI =& get_instance();
        $this->CI->load->database(); 
    }

    /** SEND ALL EMAILS  FUNCTION **/
    function send_mail($toemail, $subject, $message, $attachment = null, $fromemail = null)
    {
        $cimail = & get_instance();
        $cimail->load->library('parser');
        $mailconfig = $cimail->config->item('ci_astro');
        $fromemail = (!empty($fromemail)) ? $fromemail : $mailconfig['email']['from_main_email'];
        $cimail->load->library('email');
        $cimail->email->set_newline("\r\n");
        $cimail->email->set_mailtype("html");
        $cimail->email->from($fromemail,$mailconfig['site_title']); // change it to yours  $mailconfig['email']['from_email'],$mailconfig['email']['from_name']); 
        //$cimail->email->to($toemail);// change it to yours
        $cimail->email->bcc($toemail);// change it to yours
        

        $cimail->email->subject($subject);
        $cimail->email->message($message);
        if($cimail->email->send())
        {
            return true;
        }
        else
        {
            //return false;
            show_error($cimail->email->print_debugger());die;
        } 
    }

   
}
