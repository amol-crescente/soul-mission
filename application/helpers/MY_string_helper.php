<?php
/**
 * Helper class to work with string
 */

// check whether a string starts with the target substring
function starts_with($haystack, $needle)
{
	return substr($haystack, 0, strlen($needle))===$needle;
}

// check whether a string ends with the target substring
function ends_with($haystack, $needle)
{
	return substr($haystack, -strlen($needle))===$needle;
}

// ($files can be string or string array)
function add_script($files, $append = TRUE, $position = 'foot')
{
	$CI       = & get_instance();
  $config   = $CI->config->item('ci_justdiidz');
  $files    = is_string($files) ? array($files) : $files;
	$position = ($position==='head' || $position==='foot') ? $position : 'foot';

	if ($append)
		$config['scripts'][$position] = array_merge($config['scripts'][$position], $files);
	else
		$config['scripts'][$position] = array_merge($files, $config['scripts'][$position]);
}   

// Add stylesheet files, either append or prepend to $config array
// ($files can be string or string array)
function add_stylesheet($files, $append = TRUE, $media = 'screen')
{
	$CI       = & get_instance();
  $config   = $CI->config->item('ci_justdiidz');
  $files    = is_string($files) ? array($files) : $files;
    
  if ($append)
		$config['stylesheets'][$media] = array_merge($config['stylesheets'][$media], $files);
	else
		$config['stylesheets'][$media] = array_merge($files, $config['stylesheets'][$media]);
}

function get_object($table, $select, $key, $value, $returntype=null, $exwhere = null, $order = null)
{
   $options ="";
   //get main CodeIgniter object
   $ci =& get_instance();
   //load databse library
   $ci->load->database();
   //get data from database
   $ci->db->select($select);
   if($key && $value)
   {
   $ci->db->where(array($key => $value)); 
   }

   if($exwhere)
   {
	   $ci->db->where($exwhere);   
   }

   if($order)
   {
	   $ci->db->order_by($order);   
   }

   $query = $ci->db->get($table);
  // echo $ci->db->last_query();die;
   $objectinfo = $query->row_object();

   return $returntype==1?$objectinfo:$objectinfo->$select;
}

function get_objectlist($table, $select, $exwhere = null)
{
   $options ="";
   //get main CodeIgniter object
   $ci =& get_instance();
   //load databse library
   $ci->load->database();
   //get data from database
   $ci->db->select($select);
   if($exwhere)
   {
	   $ci->db->where($exwhere);   
   }

   $query = $ci->db->get($table);
   $result = $query->result_array();
   //echo $ci->db->last_query();die;
   return $result;
}

function get_dbconfig($value,$account_id)
{
   $options ="";
   //get main CodeIgniter object
   $ci =& get_instance();
   //load databse library
   $ci->load->database();
   //get data from database
   $ci->db->select('value');
   $ci->db->where(array('key' => $value, 'account_id'=>$account_id));
   $query = $ci->db->get(TB_PARAMETERS);
   $objectinfo = $query->row_object();
   return $objectinfo->value;
}

function get_join_object($table1, $table2, $on, $jointype, $select, $exwhere, $resulttype)
{
   $options ="";
   //get main CodeIgniter object
   $ci =& get_instance();
   //load databse library
   $ci->load->database();
   //get data from database
   $ci->db->select($select);
   $ci->db->from($table1);
   $ci->db->join($table2, $on, $jointype);
   $ci->db->where($exwhere);
   $query = $ci->db->get();   
   $objectinfo = $resulttype ==1 ? $query->row_object() : $query->result();   
   return $objectinfo;
}

function get_join_list($table1, $table2, $on, $jointype, $select, $exwhere)
{
   $options ="";
   //get main CodeIgniter object
   $ci =& get_instance();
   //load databse library
   $ci->load->database();
   //get data from database
   $ci->db->select($select);
   $ci->db->from($table1);
   $ci->db->join($table2, $on, $jointype);
   $ci->db->where($exwhere);
   $query = $ci->db->get();   
   //echo $ci->db->last_query();die;
   $objectinfo = $query->result_array();
   return $objectinfo;
}

/** REMOVE RECORD FROM DB **/
function remove_item($table,$where)
{
    $ci =& get_instance();
    $ci->db->where($where);
    $delete = $ci->db->delete($table);
    return $delete;
}

function getExtension($str) 
{
  $i = strrpos($str,".");
  if (!$i) { return ""; } 
  $l = strlen($str) - $i;
  $ext = substr($str,$i+1,$l);
    return $ext;
}

function strafter($string, $substring) {
  $pos = strpos($string, $substring);
  if ($pos === false)
  {
	return 0;  
  }
  else
  {
	$upstr =   substr($string, $pos+strlen($substring));  		
	$variable = strstr($upstr, ';', true);
	return !empty($variable)?$variable:$upstr;
  }
}


function set_flashmessage($flashdata=null)
{
    return '<div class="mr-b-20 alert alert-'.$flashdata['class'].'" role="alert">'.$flashdata['msg'].'</div>';
}
function addDayswithdate($date,$days)
{
    $date = strtotime("+".$days." days", ($date/1000));
    return  $date*1000;
}

function get_quantitysum($quantity)
{
  $qtylist = explode(",", $quantity);
  foreach ($qtylist as $qty) {
    $qtyarray = explode("-", $qty);
    $qtyids[]   = $qtyarray[1]; 
  }
  return array_sum($qtyids);
}
function searchForId($find, $array, $select) {
   foreach ($array as $key => $val) {
       if ($val[$select] === $find) {
           return $key;
       }
   }
   return null;
}
function rand_color() {
    return sprintf('#%06X', mt_rand(0, 0xFFFFFF));
}

