<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <meta name="description" content="">
        <meta name="author" content="">
        <title><?php echo $config['site_title'].' '.$page_title; ?></title>
        <base href="<?php echo base_url(); ?>" />
    	<?php
    		foreach ($config['meta_data'] as $name => $content)
    		{
    			if (!empty($content))
    				echo "<meta name='$name' content='$content'>".PHP_EOL;
    		}
            
            foreach ($config['stylesheets'] as $media => $files)
    		{
    			if($extracss)
                $files = array_merge($files,$extracss);
                
                foreach ($files as $file)
    			{
    				$url = starts_with($file, 'http') ? $file : base_url($file);
    				echo "<link href='$url' rel='stylesheet' media='$media'>".PHP_EOL;	
    			}
    		}
    		
    		foreach ($config['scripts']['head'] as $file)
    		{
    			$url = starts_with($file, 'http') ? $file : base_url($file);
    			echo "<script src='$url'></script>".PHP_EOL;
    		}
    	?>
        <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
          <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
          <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
        <![endif]-->
    </head>
    <body class="" id="page-top">