<!-- Begin Page Content -->
        <div class="container-fluid <?php echo $nosidebar==true ?'':'right-side';?>">

            <!-- Page Heading -->
          <div class="d-sm-flex align-items-center justify-content-between mb-4">
            <h1 class="h3 mb-0 text-gray-800"><?php echo ucfirst($page_title);?></h1>
            <?php
            /*
            <a href="#" class="d-none d-sm-inline-block btn btn-sm btn-primary shadow-sm"><i class="fas fa-download fa-sm text-white-50"></i> Generate Report</a>
          	*/
          	?>
          </div>


        <?php $this->load->view($main_content); ?>                
        </div>
<!-- End Begin Page Content -->

