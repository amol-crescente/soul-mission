<?php
    if($extrajs)
    $config['scripts']['foot'] = array_merge($config['scripts']['foot'],$extrajs);
    
    foreach ($config['scripts']['foot'] as $file)
	{
		$url = starts_with($file, 'http') ? $file : base_url($file);
		echo "<script src='$url'></script>".PHP_EOL;
	}
?>
<script type="text/javascript">
    var baseurl = "<?php print base_url(); ?>";
</script>
<!-- Footer -->
<footer class="sticky-footer bg-white">
	<div class="container my-auto">
	  <div class="copyright text-center my-auto">
	    <!-- <span>Copyright &copy; GIS APP Consultancy Services Pvt Ltd 2019</span> -->
	  </div>
	</div>
</footer>
<!-- End of Footer -->